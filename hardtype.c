#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <glib.h>
#include <cairo.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_LCD_FILTER_H

#include "override.h"

static void *FreeType = NULL;
static void *Cairo = NULL;

static cairo_font_options_t *Cairo_options = NULL;
static GHashTable *Cairo_tracker = NULL;

__attribute__((constructor)) static void start(void) {
	OVERRIDE_LOAD(FreeType, "libfreetype.so.6");
	OVERRIDE_LOAD(Cairo, "libcairo.so.2");
	OVERRIDE_CHECK(
		!, Cairo_tracker = g_hash_table_new_full(
			g_direct_hash,
			g_direct_equal,
			NULL,
			dlsym(Cairo, "cairo_font_options_destroy")
		), "g_hash_table_new_full returned %s", "NULL"
	);
	OVERRIDE_LOG_NEW(cairo_font_options_create, "%s", "");
	Cairo_options = OVERRIDE_OLD(Cairo, cairo_font_options_t *, cairo_font_options_create);
	cairo_status_t status;
	OVERRIDE_CHECK(
		CAIRO_STATUS_SUCCESS !=,
		status = OVERRIDE_OLD(Cairo, cairo_status_t, cairo_font_options_status, Cairo_options),
		"cairo_font_options_create: %s", OVERRIDE_OLD(Cairo, const char *, cairo_status_to_string, status)
	);
	OVERRIDE_LOG_NEW(cairo_font_options_set_antialias, "%p, %s", (void *) Cairo_options, "SUBPIXEL");
	OVERRIDE_OLD(Cairo, void, cairo_font_options_set_antialias, Cairo_options, CAIRO_ANTIALIAS_SUBPIXEL);
	OVERRIDE_LOG_NEW(cairo_font_options_set_subpixel_order, "%p, %s", (void *) Cairo_options, "RGB");
	OVERRIDE_OLD(Cairo, void, cairo_font_options_set_subpixel_order, Cairo_options, CAIRO_SUBPIXEL_ORDER_RGB);
	OVERRIDE_LOG_NEW(cairo_font_options_set_hint_style, "%p, %s", (void *) Cairo_options, "SLIGHT");
	OVERRIDE_OLD(Cairo, void, cairo_font_options_set_hint_style, Cairo_options, CAIRO_HINT_STYLE_SLIGHT);
	OVERRIDE_LOG_NEW(cairo_font_options_set_hint_metrics, "%p, %s", (void *) Cairo_options, "ON");
	OVERRIDE_OLD(Cairo, void, cairo_font_options_set_hint_metrics, Cairo_options, CAIRO_HINT_METRICS_ON);
}

OVERRIDE_NEW(FT_Error, FT_Load_Glyph, FT_Face face, FT_UInt glyph_index, FT_Int32 load_flags) {
	unsigned long flags = (unsigned long) load_flags;
	OVERRIDE_LOG_CHANGES(
		{
			flags &= ~FT_LOAD_NO_HINTING;
			flags |=  FT_LOAD_FORCE_AUTOHINT;
			flags &= ~FT_LOAD_NO_AUTOHINT;
			flags &= ~FT_LOAD_MONOCHROME;
			flags &= ~FT_LOAD_TARGET_(0xF);
			flags |=  FT_LOAD_TARGET_LIGHT;
		},
		FT_Load_Glyph,
		"%p, 0x%X, %s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
		(void *) face,
		glyph_index,
		((flags >> 16  & 15) == FT_RENDER_MODE_NORMAL) ? "NORMAL" :
		((flags >> 16  & 15) ==  FT_RENDER_MODE_LIGHT) ? "LIGHT" :
		((flags >> 16  & 15) ==   FT_RENDER_MODE_MONO) ? "MONO" :
		((flags >> 16  & 15) ==    FT_RENDER_MODE_LCD) ? "LCD" :
		((flags >> 16  & 15) ==  FT_RENDER_MODE_LCD_V) ? "LCD_V" :
		"???",
		(flags & 1UL <<  0) ? " | NO_SCALE" : "",
		(flags & 1UL <<  1) ? " | NO_HINTING" : "",
		(flags & 1UL <<  2) ? " | RENDER" : "",
		(flags & 1UL <<  3) ? " | NO_BITMAP" : "",
		(flags & 1UL <<  4) ? " | VERTICAL_LAYOUT" : "",
		(flags & 1UL <<  5) ? " | FORCE_AUTOHINT" : "",
		(flags & 1UL <<  6) ? " | CROP_BITMAP" : "",
		(flags & 1UL <<  7) ? " | PEDANTIC" : "",
		(flags & 1UL <<  9) ? " | IGNORE_GLOBAL_ADVANCE_WIDTH" : "",
		(flags & 1UL << 10) ? " | NO_RECURSE" : "",
		(flags & 1UL << 11) ? " | IGNORE_TRANSFORM" : "",
		(flags & 1UL << 12) ? " | MONOCHROME" : "",
		(flags & 1UL << 13) ? " | LINEAR_DESIGN" : "",
		(flags & 1UL << 15) ? " | NO_AUTOHINT" : "",
		(flags & 1UL << 20) ? " | COLOR" : "",
		(flags & 1UL << 21) ? " | COMPUTE_METRICS" : ""
	);
	return OVERRIDE_OLD(FreeType, FT_Error, FT_Load_Glyph,
	                    face, glyph_index, (FT_Int32) flags);
}

/* FT_Render_Glyph is no longer directly overridden by default, as
   there are many applications that, surprise surprise, don't like it
   when they ask for a certain kind of rendering, with the layout and
   size of buffer that it implies, and have that request ignored.

   Firefox starts to black out large parts of the viewport, flicker
   aggressively, or skip drawing text altogether on some (usually
   complex) pages and elements. This consistently includes Facebook
   videos and the menus on the pfSense web interface.

   Firefox may throw errors along the lines of:

           Crash Annotation GraphicsCriticalError: |[0][GFX1-]: \
           DrawSurface with bad surface 1|[921][GFX1-]: Attempt to \
           create DrawTarget for invalid surface. Size(1600,803)

   PDF.js and Atril (MATE's Evince) completely fail to render at least
   some documents, while Qt applications, and some parts of Java
   applications, often draw glyphs in a stretched or squashed way.

   On the other hand, Android Studio is completely fine with this, as
   is ChemDoodle for the most part, except for e.g. the splash screen
   and the small blue "n" glyphs in the primary toolbar on the left.

   I'd imagine that FT_LOAD_MONOCHROME and FT_LOAD_TARGET_* have a
   similar potential to cause problems like these if they are
   overridden, but I haven't had any problems yet (famous last words).

   I'll investigate selectively overriding FT_Render_Glyph based on
   program heuristics or FT_Load_Glyph/FT_Render_Glyph call patterns,
   but for the most part, it looks like subpixel smoothing will need
   to be overridden in the functions that are below FreeType in the
   call stack, e.g. cairo_font_options_set_alias for Cairo. */

OVERRIDE_NEW(FT_Error, FT_Render_Glyph, FT_GlyphSlot slot, FT_Render_Mode render_mode) {
	/*** OVERRIDE_LOG_CHANGES(
		render_mode = FT_RENDER_MODE_LCD,
		...
	); ***/
	OVERRIDE_LOG_CALL(
		FT_Render_Glyph,
		"%p, %s",
		(void *) slot,
		(render_mode == FT_RENDER_MODE_NORMAL) ? "NORMAL" :
		(render_mode ==  FT_RENDER_MODE_LIGHT) ? "LIGHT" :
		(render_mode ==   FT_RENDER_MODE_MONO) ? "MONO" :
		(render_mode ==    FT_RENDER_MODE_LCD) ? "LCD" :
		(render_mode ==  FT_RENDER_MODE_LCD_V) ? "LCD_V" :
		"???"
	);
	return OVERRIDE_OLD(FreeType, FT_Error, FT_Render_Glyph,
	                    slot, render_mode);
}

/* Leaving this enabled, regardless of whether or not FT_Render_Glyph
   ends up being called with FT_RENDER_MODE_LCD some or all of the
   time, shouldn't cause any problems. The LCD filter is only "applied
   to subpixel-rendered bitmaps generated through FT_Render_Glyph". */

OVERRIDE_NEW(FT_Error, FT_Library_SetLcdFilter, FT_Library library, FT_LcdFilter filter) {
	OVERRIDE_LOG_CHANGES(
		filter = FT_LCD_FILTER_DEFAULT,
		FT_Library_SetLcdFilter,
		"%p, %s",
		(void *) library,
		(filter ==    FT_LCD_FILTER_NONE) ? "NONE" :
		(filter == FT_LCD_FILTER_DEFAULT) ? "DEFAULT" :
		(filter ==   FT_LCD_FILTER_LIGHT) ? "LIGHT" :
		(filter ==  FT_LCD_FILTER_LEGACY) ? "LEGACY" :
		(filter == FT_LCD_FILTER_LEGACY1) ? "LEGACY1" :
		"???"
	);
	return OVERRIDE_OLD(FreeType, FT_Error, FT_Library_SetLcdFilter,
	                    library, filter);
}

OVERRIDE_NEW(FT_Error, FT_Init_FreeType, FT_Library *library) {
	OVERRIDE_LOG_OLD(FT_Init_FreeType, "%p", (void *) library);
	OVERRIDE_LOG_NEW(FT_Init_FreeType, "%p", (void *) library);
	/* We only ever return an error value if it comes from
	   FT_Init_FreeType, not FT_Library_SetLcdFilter, just in case
	   the calling code specifically expects each error that it
	   receives to be an error that the former could return. */
	FT_Error error = OVERRIDE_OLD(FreeType, FT_Error, FT_Init_FreeType,
	                              library);
	if (!error) {
		FT_LcdFilter filter = FT_LCD_FILTER_DEFAULT;
		OVERRIDE_LOG_NEW(FT_Library_SetLcdFilter,
		                 "%p, FT_LCD_FILTER_DEFAULT",
		                 (void *) library);
		OVERRIDE_OLD(FreeType, FT_Error, FT_Library_SetLcdFilter,
		             *library, filter);
	}
	return error;
}

OVERRIDE_NEW(void, cairo_font_options_set_antialias, cairo_font_options_t *options, cairo_antialias_t antialias) {
	OVERRIDE_LOG_CHANGES(
		antialias = CAIRO_ANTIALIAS_SUBPIXEL,
		cairo_font_options_set_antialias,
		"%p, %s",
		(void *) options,
		(antialias ==  CAIRO_ANTIALIAS_DEFAULT) ? "DEFAULT" :
		(antialias ==     CAIRO_ANTIALIAS_NONE) ? "NONE" :
		(antialias ==     CAIRO_ANTIALIAS_GRAY) ? "GRAY" :
		(antialias == CAIRO_ANTIALIAS_SUBPIXEL) ? "SUBPIXEL" :
		(antialias ==     CAIRO_ANTIALIAS_FAST) ? "FAST" :
		(antialias ==     CAIRO_ANTIALIAS_GOOD) ? "GOOD" :
		(antialias ==     CAIRO_ANTIALIAS_BEST) ? "BEST" :
		"???"
	);
	return OVERRIDE_OLD(Cairo, void, cairo_font_options_set_antialias, options, antialias);
}

OVERRIDE_NEW(void, cairo_font_options_set_subpixel_order, cairo_font_options_t *options, cairo_subpixel_order_t order) {
	OVERRIDE_LOG_CHANGES(
		order = CAIRO_SUBPIXEL_ORDER_RGB,
		cairo_font_options_set_subpixel_order,
		"%p, %s",
		(void *) options,
		(order == CAIRO_SUBPIXEL_ORDER_DEFAULT) ? "DEFAULT" :
		(order ==     CAIRO_SUBPIXEL_ORDER_RGB) ? "RGB" :
		(order ==     CAIRO_SUBPIXEL_ORDER_BGR) ? "BGR" :
		(order ==    CAIRO_SUBPIXEL_ORDER_VRGB) ? "VRGB" :
		(order ==    CAIRO_SUBPIXEL_ORDER_VBGR) ? "VBGR" :
		"???"
	);
	return OVERRIDE_OLD(Cairo, void, cairo_font_options_set_subpixel_order, options, order);
}

OVERRIDE_NEW(void, cairo_font_options_set_hint_style, cairo_font_options_t *options, cairo_hint_style_t style) {
	OVERRIDE_LOG_CHANGES(
		style = CAIRO_HINT_STYLE_SLIGHT,
		cairo_font_options_set_hint_style,
		"%p, %s",
		(void *) options,
		(style == CAIRO_HINT_STYLE_DEFAULT) ? "DEFAULT" :
		(style ==    CAIRO_HINT_STYLE_NONE) ? "NONE" :
		(style ==  CAIRO_HINT_STYLE_SLIGHT) ? "SLIGHT" :
		(style ==  CAIRO_HINT_STYLE_MEDIUM) ? "MEDIUM" :
		(style ==    CAIRO_HINT_STYLE_FULL) ? "FULL" :
		"???"
	);
	return OVERRIDE_OLD(Cairo, void, cairo_font_options_set_hint_style, options, style);
}

OVERRIDE_NEW(void, cairo_font_options_set_hint_metrics, cairo_font_options_t *options, cairo_hint_metrics_t behaviour) {
	OVERRIDE_LOG_CHANGES(
		behaviour = CAIRO_HINT_METRICS_ON,
		cairo_font_options_set_hint_metrics,
		"%p, %s",
		(void *) options,
		(behaviour == CAIRO_HINT_METRICS_DEFAULT) ? "DEFAULT" :
		(behaviour ==     CAIRO_HINT_METRICS_OFF) ? "OFF" :
		(behaviour ==      CAIRO_HINT_METRICS_ON) ? "ON" :
		"???"
	);
	return OVERRIDE_OLD(Cairo, void, cairo_font_options_set_hint_metrics, options, behaviour);
}

OVERRIDE_NEW(void, cairo_show_text, cairo_t *target, const char *string) {
	OVERRIDE_LOG_OLD(cairo_show_text, "%p, \"%s\"", (void *) target, string);
	cairo_font_options_t *options;
	if ((options = g_hash_table_lookup(Cairo_tracker, target))) {
		OVERRIDE_LOG_NEW(cairo_set_font_options, "%p, %p", (void *) target, (void *) options);
		OVERRIDE_OLD(Cairo, void, cairo_set_font_options, target, options);
	}
	OVERRIDE_LOG_NEW(cairo_show_text, "%p, \"%s\"", (void *) target, string);
	OVERRIDE_OLD(Cairo, void, cairo_show_text, target, string);
}

OVERRIDE_NEW(void, cairo_show_glyphs, cairo_t *target, const cairo_glyph_t *glyphs, int count) {
	OVERRIDE_LOG_OLD(cairo_show_glyphs, "%p, %p, %d", (void *) target, (void *) glyphs, count);
	cairo_font_options_t *options;
	if ((options = g_hash_table_lookup(Cairo_tracker, target))) {
		OVERRIDE_LOG_NEW(cairo_set_font_options, "%p, %p", (void *) target, (void *) options);
		OVERRIDE_OLD(Cairo, void, cairo_set_font_options, target, options);
	}
	OVERRIDE_LOG_NEW(cairo_show_glyphs, "%p, %p, %d", (void *) target, (void *) glyphs, count);
	OVERRIDE_OLD(Cairo, void, cairo_show_glyphs, target, glyphs, count);
}

OVERRIDE_NEW(void, cairo_show_text_glyphs,
             cairo_t *target, const char *string, int count_string,
             const cairo_glyph_t *glyphs, int count_glyphs,
             const cairo_text_cluster_t *clusters, int count_clusters,
             cairo_text_cluster_flags_t flags) {
	OVERRIDE_LOG_OLD(cairo_show_text_glyphs,
	                 "%p, \"%s\", %d, %p, %d, %p, %d, %s",
	                 (void *) target,
	                 string, count_string,
	                 (void *) glyphs, count_glyphs,
	                 (void *) clusters, count_clusters,
	                 (flags & CAIRO_TEXT_CLUSTER_FLAG_BACKWARD) ? "BACKWARD" :
	                 (flags == 0) ? "0" :
	                 "???");
	cairo_font_options_t *options;
	if ((options = g_hash_table_lookup(Cairo_tracker, target))) {
		OVERRIDE_LOG_NEW(cairo_set_font_options, "%p, %p", (void *) target, (void *) options);
		OVERRIDE_OLD(Cairo, void, cairo_set_font_options, target, options);
	}
	OVERRIDE_LOG_NEW(
		cairo_show_text_glyphs, "%p, \"%s\", %d, %p, %d, %p, %d, %s",
		(void *) target, string, count_string, (void *) glyphs, count_glyphs,
		(void *) clusters, count_clusters,
		(flags & CAIRO_TEXT_CLUSTER_FLAG_BACKWARD) ? "BACKWARD" : (flags == 0) ? "0" : "???"
	);
	OVERRIDE_OLD(
		Cairo, void, cairo_show_text_glyphs,
		target, string, count_string, glyphs, count_glyphs,
		clusters, count_clusters, flags
	);
}

OVERRIDE_NEW(void, cairo_destroy, cairo_t *target) {
	OVERRIDE_LOG_OLD(cairo_destroy, "%p", (void *) target);
	if (OVERRIDE_OLD(Cairo, unsigned int, cairo_get_reference_count, target) == 1) {
		cairo_font_options_t *options;
		if ((options = g_hash_table_lookup(Cairo_tracker, target))) {
			OVERRIDE_LOG_NEW(cairo_font_options_destroy, "%p", (void *) options);
			g_hash_table_remove(Cairo_tracker, options);
		}
	}
	OVERRIDE_LOG_NEW(cairo_destroy, "%p", (void *) target);
	OVERRIDE_OLD(Cairo, void, cairo_destroy, target);
}

OVERRIDE_NEW(cairo_t *, cairo_create, cairo_surface_t *target) {
	OVERRIDE_LOG_OLD(cairo_create, "%p", (void *) target);
	OVERRIDE_LOG_NEW(cairo_create, "%p", (void *) target);
	cairo_t *result = OVERRIDE_OLD(Cairo, cairo_t *, cairo_create, target);
	OVERRIDE_LOG_NEW(cairo_font_options_copy, "%p", Cairo_options);
	cairo_font_options_t *options = OVERRIDE_OLD(Cairo, cairo_font_options_t *, cairo_font_options_copy, Cairo_options);
	if (CAIRO_STATUS_SUCCESS == OVERRIDE_OLD(Cairo, cairo_status_t, cairo_font_options_status, options)) {
		OVERRIDE_LOG_NEW(cairo_set_font_options, "%p, %p", (void *) result, (void *) options);
		OVERRIDE_OLD(Cairo, void, cairo_set_font_options, result, options);
		g_hash_table_insert(Cairo_tracker, result, options);
	}
	return result;
}

/*      OVERRIDE_CHECK(PREFIX, CALL, FORMAT, VALUES...)
   e.g. OVERRIDE_CHECK(!, malloc(13), "malloc: %s", strerror(errno));

   Makes the given function call, and if the value it returns is
   considered truthy when placed after the given prefix, prints the
   given error message to standard error, and then aborts.

   PREFIX: any sequence of tokens that may go before an expression
   CALL: function call expression (or really, any expression)
   FORMAT: string literal
   VALUES...: expressions */

#define OVERRIDE_CHECK(PREFIX, CALL, FORMAT, ...) do { \
	if (PREFIX (CALL)) { \
		fprintf(stderr, \
		        __FILE__ ": " FORMAT "\n", \
		        __VA_ARGS__); \
		abort(); \
	} \
} while (0)

/*      OVERRIDE_LOAD(LIBRARY_NAME, LIBRARY_PATH)
   e.g. OVERRIDE_LOAD(FreeType, "libfreetype.so.6")

   Opens the given dynamic library at a previously declared variable.
   Aborts if dlopen(3) fails.

   LIBRARY_NAME: identifier
   LIBRARY_PATH: (const char *) */

#define OVERRIDE_LOAD(LIBRARY_NAME, LIBRARY_PATH) do { \
	OVERRIDE_CHECK(!, LIBRARY_NAME = dlopen( \
	                       LIBRARY_PATH, RTLD_LAZY | RTLD_LOCAL \
	               ), "dlopen: %s", dlerror()); \
} while (0)

/*      OVERRIDE_NEW(RETURN_TYPE, SYMBOL_NAME, PARAMETERS...)
   e.g. OVERRIDE_NEW(void *, calloc, size_t count, size_t each);
   e.g. OVERRIDE_NEW(void *, malloc, size_t size) { ... }

   Yield a declaration for a function that is to be overridden,
   possibly followed by a body, which would create a definition.

   RETURN_TYPE:     type expression
   SYMBOL_NAME:     identifier
   PARAMETERS...:   formal parameters */

#define OVERRIDE_NEW(RETURN_TYPE, SYMBOL_NAME, ...) \
        extern RETURN_TYPE SYMBOL_NAME(__VA_ARGS__)

/*      OVERRIDE_OLD(LIBRARY, RETURN_TYPE, SYMBOL_NAME, ARGUMENTS...)
   e.g. void *buffer = OVERRIDE_OLD(C, void *, calloc, 13, 4096);
   e.g. if (!OVERRIDE_OLD(C, int, strcmp, argv[i], "-version"))

   Yield a call to the original version of a function in the given
   dynamic library, using a handle that was returned by dlopen(3).

   This macro casts the return value of dlsym(3) to a pointer to a
   function that accepts unspecified arguments, in order to save you
   from having to copy the formal parameters from OVERRIDE_NEW.

   As far as I can tell, this doesn't break any functions or their
   calling conventions, but you have been warned.

   LIBRARY:         (const void *)
   RETURN_TYPE:     type expression
   SYMBOL_NAME:     identifier
   ARGUMENTS...:    expressions */

#define OVERRIDE_OLD(LIBRARY, RETURN_TYPE, SYMBOL_NAME, ...) \
        (* (RETURN_TYPE (*) ()) dlsym(LIBRARY, #SYMBOL_NAME)) \
        (__VA_ARGS__)

/*      OVERRIDE_LOG_CALL(SYMBOL_NAME, FORMAT_STRING, VALUES...)
   e.g. OVERRIDE_LOG_CALL(calloc, "%zu, %zu", 13, 4096);
     -> foo.c: calloc(13, 4096)

   Logs a call to standard error. The function is not invoked.

   If NDEBUG is defined, do nothing.

   SYMBOL_NAME: identifier
   FORMAT_STRING: string literal
   VALUES...: expressions */

#define OVERRIDE_LOG_CALL(...) do { \
	OVERRIDE_LOG_HELPER("", __VA_ARGS__); \
} while (0)

/*      OVERRIDE_LOG_NEW(SYMBOL_NAME, FORMAT_STRING, VALUES...)
   e.g. OVERRIDE_LOG_NEW(calloc, "%zu, %zu", 13, 4096);
     -> foo.c: new: calloc(13, 4096) */

#define OVERRIDE_LOG_NEW(...) do { \
	OVERRIDE_LOG_HELPER("new: ", __VA_ARGS__); \
} while (0)

/*      OVERRIDE_LOG_OLD(SYMBOL_NAME, FORMAT_STRING, VALUES...)
   e.g. OVERRIDE_LOG_OLD(calloc, "%zu, %zu", 13, 4096);
     -> foo.c: old: calloc(13, 4096) */

#define OVERRIDE_LOG_OLD(...) do { \
	OVERRIDE_LOG_HELPER("old: ", __VA_ARGS__); \
} while (0)

/*      OVERRIDE_LOG_CHANGES(CHANGES, SYMBOL_NAME, FORMAT, VALUES...)
   e.g. OVERRIDE_LOG_CHANGES({ size *= 2; }, malloc, "%zu", size);
     -> foo.c: old: calloc(32768)
     -> foo.c: new: malloc(65536)

   Logs a call to standard error, before and after the given changes
   or other statements are made. The function is not invoked.

   If NDEBUG is defined, yield no logging code, only the changes.

   The changes may be in the form of an expression, statement, or even
   a block, but as they are passed as a macro argument, the changes
   must not contain any commas that aren't "protected" by parentheses.

   CHANGES: statements
   SYMBOL_NAME: identifier
   FORMAT: string literal
   VALUES...: expressions */

#define OVERRIDE_LOG_CHANGES(CHANGES, ...) do { \
	OVERRIDE_LOG_OLD(__VA_ARGS__); \
	CHANGES; \
	OVERRIDE_LOG_NEW(__VA_ARGS__); \
} while (0)

/*      OVERRIDE_LOG_HELPER(PREFIX, SYMBOL_NAME, FORMAT, VALUES...)
   e.g. OVERRIDE_LOG_HELPER("ignored: ", free, "%p", buffer);
     -> foo.c: ignored: free(0x13131313)

   Logs a call to standard error, including the given prefix string.
   The function is not invoked.

   If NDEBUG is defined, do nothing.

   I had a choice between implementing this macro in an atomic way on
   POSIX systems, and allowing the format string to be an expression
   that isn't a (sequence of) string literals. I chose the former.

   You must increment any argument indices in the format string, when
   using the XSI extensions %n$ and/or *m$.

   PREFIX: (const char *)
   SYMBOL_NAME: identifier
   FORMAT: string literal
   VALUES...: expressions */

#ifndef NDEBUG

	#define OVERRIDE_LOG_HELPER(PREFIX, SYMBOL_NAME, FORMAT, ...) do { \
		fprintf(stderr, \
		        __FILE__ ": %s" #SYMBOL_NAME \
		        "(" FORMAT ")\n", \
		        PREFIX, __VA_ARGS__); \
	} while (0)

#else

	#define OVERRIDE_LOG_HELPER(PREFIX, SYMBOL_NAME, FORMAT, ...)

#endif

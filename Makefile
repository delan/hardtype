.POSIX:

DEBUG   := -DNDEBUG

IN      := hardtype
OUT     := libhardtype.so.0
PC_C    := glib-2.0 freetype2 cairo
PC_L    := glib-2.0

CFLAGS  := -fPIC -g -std=c99 -Wall $(DEBUG)
CFLAGS  := $(CFLAGS) $$(pkg-config --cflags $(PC_C))
LDFLAGS := -shared -soname $(OUT)
LDLIBS  := -lc -ldl $$(pkg-config --libs $(PC_L))

$(OUT): $(IN).o
	$(LD) $(LDFLAGS) -o $(OUT) $(IN).o $(LDLIBS)

$(IN).o: $(IN).c override.h
	$(CC) $(CFLAGS) -c -o $(IN).o $(IN).c

install:
	cp $(OUT) /usr/local/lib
	ldconfig

clean:
	rm -f $(OUT) $(IN).o

.PHONY: install clean
